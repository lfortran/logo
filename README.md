# LFortran Logo

Logo of LFortran, in SVG and PNG formats.

## Original

The original logo is in SVG:

* [lfortran_logo.svg](lfortran_logo.svg)
* [lfortran_logo_white_bg.svg](lfortran_logo_white_bg.svg)

The first one is on a transparent background, the second one is on a white
background box with round edges and transparent around it.

Below are png images of the logo in various dimensions for specific platforms.

## GitHub

Dimensions: 500px x 500px  
Filename: [lfortran_logo_white_bg_500x500.png](lfortran_logo_white_bg_500x500.png)

The white background, converted to png.

## GitLab

Dimensions: 500px x 500px  
Filename: [lfortran_logo_white_bg_500x500.png](lfortran_logo_white_bg_500x500.png)

The same file as for GitHub.

## Twitter

Dimensions: 400px x 400px

## Favicon

Dimensions: 32px x 32px
